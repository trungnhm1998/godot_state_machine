extends StateMachine

class_name PlayerStateMachine

func init_player_state_machine(player: Player):
	var _states = {
		"idle": player.get_node("state_machine/idle"),
		"crouch": player.get_node("state_machine/crouch"),
		"jump": player.get_node("state_machine/jump"),
		"walk": player.get_node("state_machine/walk"),
		"crouch_walk": player.get_node("state_machine/crouch_walk"),
	}
	for state in _states.values():
		state._player = player
	concurrent_states = ["walk", "crouch", "crouch_walk"]
	add_state(_states.idle)
	add_state(_states.crouch)
	add_state(_states.jump)
	add_state(_states.walk)
	add_state(_states.crouch_walk)
	.init(_states.idle)
