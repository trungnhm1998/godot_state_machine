extends State

class_name PlayerState

export (float) var move_speed = 0.0
var _player: Player = null
var _direction = Vector2()
var _is_down_pressed = false

func _enter(previous_state):
	_player.get_node("state_label").text = "state: %s" % name
	return

func _process_state(delta):
	_direction = Vector2()
	_direction.x = int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left"))
	_is_down_pressed = Input.is_action_pressed("down")
	if _player != null:
		if _direction.x == 1:
			_player.get_node("sprite").set_flip_h(false)
		elif _direction.x == -1:
			_player.get_node("sprite").set_flip_h(true)

	_player.get_node("direction_label").text = "direction: %s" % _direction